import json
from operator import itemgetter
import numpy as np
import gzip
import time

class Batches:

    def __init__(self, file_name, batch_size, ratio = None):
        self.batch_size = batch_size
        self.f = gzip.open(file_name, 'r')
        self.ratio = ratio

    def create_batch(self):
        if self.ratio == None:
            return self.simple_create()
        else:
            return self.ratio_create()

    def simple_create(self):
        batch_docs = np.zeros((self.batch_size,10,20480))
        batch_queries = np.zeros((self.batch_size,1024))
        batch_labels = np.zeros((self.batch_size,10))
        batch_ll = np.zeros((self.batch_size,10,11))
        session_id = []
        query_id = []
        for i in xrange(self.batch_size):
            line = self.f.readline()
            line = line.rstrip()
            x = json.loads(line)
            session_id.append(x['search_session_id'])
            query_id.append(x['query_id'])
            q,d,l,ll,_ = self.dict_to_feature(x)
            batch_docs[i] = d
            batch_queries[i] = q
            batch_labels[i] = l
            batch_ll[i] = ll

        return query_id, session_id, batch_queries, batch_docs, batch_labels, batch_ll

    def ratio_create(self):
        seq_added = 0
        nonseq_added = 0
        seq = int(self.batch_size * self.ratio)
        nonseq = self.batch_size - seq
        batch_docs = np.zeros((self.batch_size,10,20480))
        batch_queries = np.zeros((self.batch_size,1024))
        batch_labels = np.zeros((self.batch_size,10))
        batch_ll = np.zeros((self.batch_size,10,11))
        while seq_added < seq or nonseq_added < nonseq:
            line = self.f.readline()
            line = line.rstrip()
            x = json.loads(line)
            q,d,l,ll, sequential = self.dict_to_feature(x)
            if ((sequential and seq_added < seq) or (not sequential and nonseq_added < nonseq)) and len(x['clicks'])>0:
                i = seq_added + nonseq_added
                batch_docs[i] = d
                batch_queries[i] = q
                batch_labels[i] = l
                batch_ll[i] = ll
                if sequential:
                    seq_added += 1
                else:
                    nonseq_added += 1
        return batch_queries, batch_docs, batch_labels, batch_ll

    def close_file(self):
        self.f.close()

    def dict_to_feature(self,d):
        bf = d['behavioral_features']
        # Creating query representation
        d_query = np.zeros(1024)
        bf_q = bf['Q']
        for key,value in bf_q.iteritems():
            d_query[int(key)] = int(value)
        # Creating labels
        d_labels = np.asarray(d['click_pattern'])
        # Creating Document representation
        d_documents = np.zeros((10,20480))
        # Representation d1
        bf_qd = bf['QD']
        for i in range(0,10):
            for key,value in bf_qd[i].iteritems():
                d_documents[i,int(key)] = int(value)
        t = time.time()
        #Representation d3
        bf_d = bf['D']
        for i in range(0,10):
            for key,value in bf_d[i].iteritems():
                d_documents[i,int(key)+10240] = int(value)
        clicks = d['clicks']
        c_dict = {}
        ll = []
        if len(clicks) > 0:
            for item in clicks:
                pos = item['position']
                c_dict[pos] = item['timestamp']
            sorted_clicks = sorted(c_dict.items(),key=itemgetter(1))
            sc = map(list,zip(*sorted_clicks))[0]
            sequential = self.is_sequential(sc)
            loss_label = self.create_loss_labels(sc)
        else:
            sequential = False
            loss_label = self.create_loss_labels([])
        ll.append(loss_label)
        d_query = np.log(np.asarray(d_query)+1)
        d_documents = np.log(np.asarray(d_documents)+1)
        return d_query, d_documents, d_labels, np.asarray(ll), sequential

    def create_loss_labels(self,l):
        no_click = np.zeros(11)
        no_click[10] = 1.0
        no_click = no_click.tolist()
        ll = []
        i=-1
        for i in xrange(len(l)):
            pos = l[i]
            vec = np.zeros(11)
            vec = vec.tolist()
            vec[pos] = 1.0
            ll.append(vec)
        for j in xrange(i+1,10):
            #print j
            ll.append(no_click)
        return ll

    def is_sequential(self,sequence):
        for i in range(1, len(sequence)):
            if sequence[i] < sequence[i-1]:
                return False
        return True
