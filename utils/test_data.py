import gzip
import argparse
import json
from operator import itemgetter
import matplotlib.pyplot as plt
import numpy as np


def test_data(data_file, num_sessions):
    i = 0
    zero = {0:0}
    one = {1:0}
    sequential = initialize_dict()
    non_sequential = initialize_dict()
    with gzip.open(data_file, 'rb') as f:
        while True:
            line = f.readline()
            line = line.rstrip()
            x = json.loads(line)
            clicks = x['clicks']
            sequence = clicks2sequence(clicks)
            if len(sequence) > 1:
                if is_sequential(sequence):
                    sequential[len(sequence)] += 1.0/num_sessions
                else:
                    non_sequential[len(sequence)] += 1.0/num_sessions
            elif len(sequence )== 1:
                one[1] += 1.0/num_sessions
            else:
                zero[0] += 1.0/num_sessions
            i+=1
            if num_sessions > 0 and i >= num_sessions:
                break

    width = 0.4
    plt.bar(zero.keys(), zero.values(), 2*width, color='r')
    plt.bar(one.keys(), one.values(), 2*width, color='r')
    plt.bar(sequential.keys(), sequential.values(), width, color='g', label="sequential")
    plt.xticks(np.arange(0,11))
    plt.bar(np.asarray(non_sequential.keys())+width, non_sequential.values(), width, color='b', label="non_sequential")
    plt.legend()
    plt.show()


def initialize_dict():
    sequences = {}
    for i in range(2,11):
        sequences[i] = 0
    return sequences

def clicks2sequence(clicks):
    if len(clicks) > 0:
        c_dict = {}
        for item in clicks:
            pos = item['position']
            c_dict[pos] = item['timestamp']
        sorted_clicks = sorted(c_dict.items(),key=itemgetter(1))
        sc = map(list,zip(*sorted_clicks))[0]
    else:
        sc = []
    return sc

def is_sequential(sequence):
    for i in range(1, len(sequence)):
        if sequence[i] < sequence[i-1]:
            return False
    return True

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run Sequential LSTM')
    parser.add_argument('--data', type=str, default="None", help="Data file")
    parser.add_argument('--sessions', type=int, default="100", help="Number of sessions to check")
    args = parser.parse_args()
    test_data(args.data, args.sessions)
