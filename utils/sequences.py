import numpy as np
import itertools

class Sequences:

    def __init__(self, model_type):
        self.model_type = model_type

    def get_sequences(self, n):
        if self.model_type == "Sequential":
            sequences = self.sequential_sequences(n)
        else:
            sequences = self.non_sequential_sequences(n)
        return sequences

    def sequential_sequences(self, k):
        temp_seq = [''.join(x) for x in itertools.product('01', repeat=k)]
        sequences = []
        for seq in temp_seq:
            sequences.append(np.split(np.asarray(list(seq)),k))
        sequences = np.asarray(sequences).astype(float)
        return sequences
    '''
    def non_sequential_sequences(self, n):
        z11 = np.asarray(map(int,list('{0:011b}'.format(1))))
        one_hot_vectors = np.asarray(np.eye(n))
        oh_list = one_hot_vectors.tolist()
        for oh in oh_list:
            oh.append(0.0)
        one_hot_vectos = np.asarray(oh_list)

        sequences0 = []
        for i in range(0,3):
            sequences0.append(z11)
        seq0 = np.asarray([sequences0])

        sequences1 = []
        for oh in one_hot_vectors:
            oh2 = oh.tolist()
            oh2.append(0.0)

            temp = [np.asarray(oh2)]
            for i in range(0,2):
                temp.append(z11)
            sequences1.append(temp)
        seq1 = np.asarray(sequences1)

        sequences2 = []
        perms = list(itertools.product(one_hot_vectors,repeat=2))
        perms = np.asarray(perms).tolist()
        z4 = np.zeros(n).tolist()
        for perm in perms:
            temp = []
            for p in perm:
                p.append(0.0)
                temp.append(p)
            temp.append(z11)
            temp = np.asarray(temp)
            sequences2.append(temp)
        seq2 = np.asarray(sequences2)

        perms = list(itertools.product(one_hot_vectors,repeat=3))
        new_perms = []
        for perm in perms:
            temp_perm = []
            for p in perm:
                temp_p = p.tolist()
                temp_p.append(0.0)
                temp_perm.append(temp_p)
            new_perms.append(temp_perm)
        seq3 = np.asarray(new_perms)
        sequences = np.concatenate((seq0,seq1,seq2,seq3),axis=0)
        return sequences
    '''
        # Docs can be clicked multiple times
    def non_sequential_sequences(self, n):
        z11 = np.zeros(10)
        one_hot_vectors = np.asarray(np.eye(n))
        no_click = np.expand_dims(np.repeat(np.expand_dims(z11,0),4,0),0)
        no_click = self.add_no_clics(no_click)

        one_click = []
        for oh in one_hot_vectors:
            seq = [oh,z11,z11,z11]
            one_click.append(seq)
        one_click = np.asarray(one_click)
        one_click = self.add_no_clics(one_click)

        two_clicks = []
        for oh in one_hot_vectors:
            for oh2 in one_hot_vectors:
                if not np.array_equal(oh,oh2):
                    seq = [oh,oh2,z11,z11]
                    two_clicks.append(seq)
        two_clicks = np.asarray(two_clicks)
        two_clicks = self.add_no_clics(two_clicks)

        three_clicks = []
        for oh1 in one_hot_vectors:
            for oh2 in one_hot_vectors:
                for oh3 in one_hot_vectors:
                    if not np.array_equal(oh1,oh2) and not np.array_equal(oh1,oh3) and not np.array_equal(oh2,oh3):
                        seq = [oh1, oh2, oh3, z11]
                        three_clicks.append(seq)
        three_clicks = np.asarray(three_clicks)
        three_clicks = self.add_no_clics(three_clicks)
        sequences = np.concatenate((no_click, one_click,two_clicks,three_clicks),axis=0)
        return sequences


    def add_no_clics(self, seq):
        num_seq = 4
        seq_len = 10
        num_perms = seq.shape[0]
        zeros = np.zeros((4,1))
        for i in xrange(num_seq-int(np.sum(seq[0]))):
            zeros[num_seq-(1+i)] = 1
        z = np.expand_dims(zeros,axis=0)
        z = np.repeat(z, seq.shape[0], 0)
        seq = np.concatenate((seq,z),2)
        return seq
