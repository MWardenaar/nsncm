import numpy as np

class Results:
    def __init__(self):
        self.results = {
            'loss': self.initialize_dict('loss'),
            'perplexity': self.initialize_dict('perplexity'),
            'recip_rank': self.initialize_dict('recip_rank'),
            'dist1': self.initialize_dict('dist1'),
            'dist3': self.initialize_dict('dist3'),
            'dist5': self.initialize_dict('dist5'),
            'dist10': self.initialize_dict('dist10'),
            'LCS1': self.initialize_dict('LCS1'),
            'LCS3': self.initialize_dict('LCS3'),
            'LCS5': self.initialize_dict('LCS5'),
            'LCS10': self.initialize_dict('LCS10'),
            'samples': self.initialize_dict('samples')
        }
        self.sessions = {}

    def initialize_dict(self, metric):
        d = {}
        d['seq'] = {}
        d['nonseq'] = {}
        for i in range(2,11):
            if metric == 'perplexity':
                d['seq'][i] = np.zeros(10).tolist()
                d['nonseq'][i] = np.zeros(10).tolist()
            else:
                d['seq'][i] = 0.0
                d['nonseq'][i] = 0.0
        if metric != 'perplexity':
            d['0'] = {0:0}
            d['1'] = {1:0}
        else:
            d['0'] = {0:np.zeros(10).tolist()}
            d['1'] = {1:np.zeros(10).tolist()}
        return d

    def append_results(self,r, sequence):
        lenseq = len(sequence)
        if lenseq < 2:
            key = str(lenseq)
        else:
            if self.is_sequential(sequence):
                key = 'seq'
            else:
                key = 'nonseq'

        for metric in r.keys():
            #print metric, type(r[metric])
            if type(r[metric]) == list:
                #print "jup"
                self.results[metric][key][lenseq] += r[metric]
            else:
                temp = np.asarray(self.results[metric][key][lenseq]) + np.asarray(r[metric])
                self.results[metric][key][lenseq] = temp.tolist()
        self.results['samples'][key][lenseq] += 1

    def append_session(self, query_id, session_id, likelihood, perplexity, labels, full_labels, predictions):
        self.sessions[session_id] = {
        'query_id': query_id,
        'label':labels.tolist(),
        'full_label': full_labels.tolist(),
        'predictions': predictions,
        'likelihood': float(likelihood),
        'mean_perpl': float(np.mean(perplexity)),
        'perplexity':perplexity.tolist()
        }


    def is_sequential(self,sequence):
        for i in range(1, len(sequence)):
            if sequence[i] < sequence[i-1]:
                return False
        return True

    def get_results(self):
        return self.results

    def get_sessions(self):
        return self.sessions
