import numpy as np
from dcg import DCG

class Metrics:

    def __init__(self, model_type, test_size):
        self.dcg = DCG(test_size, model_type)
        self.test_size = test_size
        self.model_type = model_type

    def reciprocal_rank(self, best_predictions, labels):
        rr = np.ones((self.test_size,1), dtype=np.float) /(len(labels[0])+1)
        for i in range(0,self.test_size):
            validation_labels = self.dcg.ordered_seq2set(labels[i])
            for j in range(0,len(best_predictions[i])):
                clicks = self.dcg.ordered_seq2set(best_predictions[i][j])
                if np.array_equal(validation_labels, clicks):
                    rr[i] = 1.0/(j+1)
                    break
        return rr

    def recall_at_r(self, recip_rank):
        recall = np.zeros((self.test_size,4))
        for i in range(0,self.test_size):
            rr = recip_rank[i]
            if rr == 1.0:
                recall[i,:] = 1.0
            elif rr >= (1.0/3):
                recall[i,1:] = 1.0
            elif rr >= 0.2:
                recall[i,2:] = 1.0
            elif rr >= 0.1:
                recall[i,3] = 1.0
        return recall

    def calculate_dcgs(self,best_predictions, labels):
        return self.dcg.calc_dcg(best_predictions, labels)

    def sequence_to_set(self, clicks):
        clicks = np.asarray(clicks)
        clicks = np.sum(clicks, axis=0)
        clicks = np.clip(clicks,0,1)
        return clicks[0:10]

    def ordered_seq2set(self, clicks):
        return self.dcg.ordered_seq2set(clicks)
