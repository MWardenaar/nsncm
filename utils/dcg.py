import numpy as np
from distance import Distance

class DCG:

    def __init__(self, validation_size, model_type):
        self.validation_size = validation_size
        self.model_type = model_type
        if self.model_type == "sequential":
            self.distance = Distance(self.model_type, sequence_length = 10)
        else:
            self.distance = Distance(self.model_type, sequence_length = 3)

    def calc_dcg(self, best_predictions, y_val):

        d_dcg_at_1 = []
        d_dcg_at_3 = []
        d_dcg_at_5 = []
        d_dcg_at_10 = []
        lcs_dcg_at_1 = []
        lcs_dcg_at_3 = []
        lcs_dcg_at_5 = []
        lcs_dcg_at_10 = []
        mean_distance = 0
        for i in xrange(self.validation_size):
            distances = []
            lcs = []
            for j in range(0,10):
                dist, l = self.distance.calculate_distance(best_predictions[i][j], np.asarray(y_val[i]))
                distances.append(dist)
                lcs.append(l)

            dcg_lcs = self.dcg_at_n(lcs)
            lcs_dcg_at_1.append(dcg_lcs[0])
            lcs_dcg_at_3.append(dcg_lcs[1])
            lcs_dcg_at_5.append(dcg_lcs[2])
            lcs_dcg_at_10.append(dcg_lcs[3])

            distances = 1.0/(np.asarray(distances) + 1)
            mean_distance += np.mean(distances)
            dcg = self.dcg_at_n(distances)
            d_dcg_at_1.append(dcg[0])
            d_dcg_at_3.append(dcg[1])
            d_dcg_at_5.append(dcg[2])
            d_dcg_at_10.append(dcg[3])
        return [d_dcg_at_1, d_dcg_at_3, d_dcg_at_5, d_dcg_at_10], [lcs_dcg_at_1, lcs_dcg_at_3, lcs_dcg_at_5, lcs_dcg_at_10]

    def dcg_at_n(self, scores):
        #dcg = (np.power(2.0,scores)-1.0)/np.log2(np.arange(2,12))
        dcg = scores/np.log2(np.arange(2,12))
        return dcg[0], np.sum(dcg[:3]), np.sum(dcg[:5]),np.sum(dcg)

    def ordered_seq2set(self, clicks):
        if len(clicks.shape) == 1: # Sequential
            return self.distance.preprocess2(clicks)
        else:
            return self.distance.preprocess(clicks)
