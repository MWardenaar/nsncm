import os
import sys
import datetime
import numpy as np
import tensorflow as tf
from utils.metrics import Metrics
from utils.batches import Batches
import argparse
import json
from operator import itemgetter
import time
import threading

from utils.results import Results
from models.sequential.sequential import NCM
from models.non_sequential_attention.attention import NS_Attention
from models.non_sequential.non_sequential import NS_NCM
from config import *

def thread_data(model_type, click_model, batches, batch_data):
    t = time.time()
    q_id, s_id, batch_queries, batch_docs, batch_seq_labels, batch_nonseq_labels  = batches.create_batch()
    t_get = time.time() - t
    t = time.time()
    if model_type == "sequential":
        data = click_model.create_data(batch_queries, batch_docs, batch_seq_labels)
        t_create = time.time()-t
        batch_data['labels'] = batch_seq_labels
        batch_data['full_labels'] = batch_nonseq_labels
    else:
        data = click_model.create_data(batch_queries, batch_docs)
        t_create = time.time()-t
        batch_data['labels'] = batch_nonseq_labels
        batch_data['full_labels'] = batch_nonseq_labels
    batch_data['q_id'] = q_id
    batch_data['s_id'] = s_id
    batch_data['q'] = batch_queries
    batch_data['d'] = batch_docs
    batch_data['data'] = data
    return batch_data

def thread_create_data(model_type, click_model, batch_queries, batch_docs, batch_labels, batch_data):
    t = time.time()
    if model_type == "sequential":
        data = click_model.create_data(batch_queries, batch_docs, batch_labels)
    else:
        data = click_model.create_data(batch_queries, batch_docs)
    batch_data['data'] = data
    batch_data['labels'] = batch_labels
    batch_data['t'] = time.time() - t

def train_op(sess, click_model, batch_data, batch_labels, batch_loss):
    t = time.time()
    l = click_model.train(sess, batch_data, batch_labels)
    batch_loss.append(l)
    batch_loss.append(time.time()-t)

def validate_op(sess, click_model, batch_data, batch_labels, batch_loss):
    #p = click_model.get_predictions(sess, batch_data, batch_labels)
    t = time.time()
    [loss,_] = click_model.get_loss(sess, batch_data, batch_labels)
    batch_loss.append(loss)
    batch_loss.append(time.time()-t)

def prediction_op(sess, click_model, batch_data, batch_labels, predictions):
    predictions.append(click_model.get_predictions( sess, batch_data, batch_labels))

def train( model_type, model):
    print model_type
    if not os.path.isdir(LOG_DIR + model_type + "/"):
        os.makedirs(LOG_DIR + model_type + "/")
    if not os.path.isdir(CHECKPOINT_DIR + model_type + "/"):
        os.makedirs(CHECKPOINT_DIR + model_type + "/")
    if not os.path.isdir(DATA_DIR):
        print("Please specify a correct data folder.")
        sys.exit()

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        if model_type == "sequential":
            click_model = NCM(BATCH_SIZE,LSTM_SIZE, max_saved = MAX_TO_KEEP)
        elif model_type == "non_sequential":
            click_model = NS_NCM(BATCH_SIZE, LSTM_SIZE, FINAL_LSTM_SIZE,max_saved = MAX_TO_KEEP)
        elif model_type == "attention":
            print "attention"
            click_model = NS_Attention(BATCH_SIZE, ATTENTION_LSTM, ATTENTION_FINAL_LSTM,max_saved = MAX_TO_KEEP)
        else:
            print("Please enter --model_type sequential or --model_type non_sequential or --model_type attention")
            sys.exit()
        if DATA_DIR == None:
            print( "No data files found")
            sys.exit()

        timestamp = "/" + datetime.datetime.now().strftime('%b-%d-%I%M%p-%G')
        train_writer = tf.summary.FileWriter(LOG_DIR + model_type + "/" + timestamp, sess.graph)
        if model == None:
            print( "\nInitializing variables")
            click_model.initialize(sess)
            save_id = 1
            begin = 0
        else:
            print("\nRestoring variables")
            save_id, begin = restore(click_model, model, sess)
            print "Save id: ", save_id

        validate(sess, click_model, train_writer, model_type, 0)

        for j in range(begin,NUM_TRAIN_FILES):
            data = DATA_DIR + "train_query_sessions_with_behavioral_features.part_" + str(j) + ".gz"

            print( "Using data: ", data)
            batches = Batches(data, BATCH_SIZE)

            t = time.time()
            mean_loss = 0
            batch_data = {}
            thread_data(model_type,click_model,batches,batch_data)
            for i in range(ITERATIONS-1):
                batch_loss = []
                t2 = threading.Thread(target=train_op, args=(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss))
                batch_data = {}
                t1 =  threading.Thread(target=thread_data, args=(model_type, click_model, batches, batch_data))
                t1.start()
                t2.start()
                t1.join()
                t2.join()
                mean_loss += batch_loss[0]
                if PRINT_PROGRESS == True and (i%PROGRESS_FREQUENCY == 0 or i == ITERATIONS-2):
                    p = (float(i)/ITERATIONS) * PROGRESS_FREQUENCY
                    print("Training: %.2f%%, loss: %.3f, time: %.2f, " % (p, mean_loss/PROGRESS_FREQUENCY, time.time()-t))
                    t = time.time()
                    mean_loss = 0
            batch_loss = []
            train_op(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss)
            print "saving"
            save = CHECKPOINT_DIR + model_type + "/" + "model_"+ str(save_id) +".ckpt"
            click_model.save_model(sess, save)
            save_id += 1
            batches.close_file()
            validate(sess, click_model, train_writer, model_type, j+1)

def restore(click_model, model, sess):
    click_model.load_model(sess, model)
    begin = int(model.split("_")[-1].strip(".ckpt"))
    save_id = begin + 1
    return save_id, begin

def validate(sess, click_model, train_writer, model_type, j):
    data = DATA_DIR + VALIDATION_FILE
    batches = Batches(data, BATCH_SIZE)
    loss = 0.0
    t = time.time()

    batch_data = {}
    thread_data(model_type,click_model,batches,batch_data)
    # if j > 0:
    #     num_iterations = VALIDATION_ITERATIONS
    # else:
    #     num_iterations = 1
    num_iterations = VALIDATION_ITERATIONS
    for i in xrange(num_iterations-1):
        batch_loss = []
        t2 = threading.Thread(target=validate_op, args=(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss))
        batch_data = {}
        t1 =  threading.Thread(target=thread_data, args=(model_type, click_model, batches, batch_data))
        t1.start()
        t2.start()
        t1.join()
        t2.join()
        loss += np.mean(batch_loss[0])
        if PRINT_PROGRESS == True and (i%PROGRESS_FREQUENCY == 0 or i == num_iterations-2):
            p = (float(i)/num_iterations) * PROGRESS_FREQUENCY
            print("Testing: %.2f%%, loss: %.3f, time: %.2f" % (p, loss/(i+1), time.time()-t))
            t = time.time()
    batch_loss = []
    validate_op(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss)
    loss += np.mean(batch_loss[0])
    mean_loss = loss/num_iterations
    summary = click_model.add_summaries(sess, mean_loss)
    train_writer.add_summary(summary, j)
    batches.close_file()

def test(model_type, model):
    print model_type
    if not os.path.isdir(OUTPUT_DIR + model_type + "/"):
        os.makedirs(OUTPUT_DIR + model_type + "/")

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    sess = tf.Session(config=config)
    with tf.Session() as sess:
        if model_type == "sequential":
            click_model = NCM(TEST_SIZE,LSTM_SIZE)
        elif model_type == "non_sequential":
            click_model = NS_NCM(TEST_SIZE, LSTM_SIZE, FINAL_LSTM_SIZE)
        elif model_type == "attention":
            click_model = NS_Attention(TEST_SIZE, ATTENTION_LSTM, ATTENTION_FINAL_LSTM)
        else:
            print("Please enter --model_type sequential or --model_type non_sequential or --model_type attention")
            sys.exit()
        if model == None:
            click_model.initialize(sess)
        else:
            click_model.load_model(sess,model)
        data_file = DATA_DIR + TEST_FILE
        batches = Batches(data_file, TEST_SIZE, ratio = RATIO)
        metrics = Metrics(model_type, TEST_SIZE)
        results = Results()
        dcg_ranks = [1,3,5,10]

        total_mean_perpl = 0
        total_rank_perpl = np.zeros(10)
        t = time.time()
        for val_iter in xrange(TEST_ITERATIONS):
            batch_data ={}
            thread_data(model_type,click_model,batches,batch_data)
            batch_loss = []
            validate_op(sess, click_model, batch_data['data'], batch_data['labels'], batch_loss)


            if FULL_TEST == True:
                sequences = click_model.get_sequences(10)
                probs = np.zeros((TEST_SIZE,10))
                scores = {}
                # Calculating full probabilities
                for count, seq in enumerate(sequences):
                    sequence_data = {}
                    thread_create_data(model_type, click_model, batch_data['q'], batch_data['d'], seq, sequence_data)
                    predictions = []
                    prediction_op(sess, click_model, sequence_data['data'], sequence_data['labels'], predictions)
                    predictions = predictions[0]

                    likelihood = click_model.likelihood(predictions, sequence_data['labels'])
                    if model_type =="sequential":
                        probs += seq*np.expand_dims(likelihood,1)
                    else:
                        probs += click_model.sequence_to_set(seq[:,0:3,:])*np.expand_dims(likelihood,1)
                    for j in xrange(TEST_SIZE):
                        if j in scores:
                            temp = scores[j]
                        else:
                            temp = {}
                        temp[count] = likelihood[j]
                        scores[j] = temp

                # Calculating perplexity
                if model_type == "sequential":
                    p = np.log2(probs)*batch_data['labels'] + ( np.log2(1.0-probs) * (1.0-batch_data['labels']) )
                else:
                    p = np.log2(probs)*click_model.sequence_to_set(batch_data['full_labels']) + ( np.log2(1.0-probs) * (1.0-click_model.sequence_to_set(batch_data['full_labels'])))

                perplexity_r = np.power(2,(-1.0/TEST_SIZE)*np.sum(p,axis=0))
                perplexity = np.mean(perplexity_r)
                total_mean_perpl += perplexity
                total_rank_perpl += perplexity_r

                if PRINT_PROGRESS == True:
                    print "mean_perpl: ", total_mean_perpl/(val_iter+1)
                    print "perpl: ", total_rank_perpl/(val_iter+1)
                # Finding best 10 sequences
                best_predictions = get_best_n(scores, 10, sequences)
                recip_rr = []
                dcg_return = []
                t1 = threading.Thread(target=get_dcg, args=(metrics,best_predictions, batch_data['full_labels'], dcg_return))
                t2 = threading.Thread(target=recip_recall, args=(metrics,best_predictions, batch_data['full_labels'], recip_rr))
                t1.start()
                t2.start()
                t1.join()
                t2.join()
                for i in range(0,TEST_SIZE):
                    dr = {}
                    dr['loss'] = batch_loss[0][i]
                    dr['perplexity'] = p[i]
                    dr['recip_rank'] = recip_rr[0][i][0]
                    for j in range(0,4):
                        dist = 'dist' + str(dcg_ranks[j])
                        lcs = "LCS" + str(dcg_ranks[j])
                        dr[dist] = dcg_return[0][j][i]
                        dr[lcs] = dcg_return[1][j][i]
                    labels = metrics.ordered_seq2set(batch_data['full_labels'][i])
                    results.append_results(dr, labels)
                    results.append_session(batch_data['q_id'][i], batch_data['s_id'][i],
                                            np.mean(batch_loss[0][i]), p[i], batch_data['labels'][i],
                                                batch_data['full_labels'][i], scores[i])

                p = (float(val_iter)/TEST_ITERATIONS) * 100
                print("Testing: %.2f%%,  time: %.2f" % (p, time.time()-t))
                with open(OUTPUT_DIR + model_type + "/results_dict", 'wb') as out_file:
                    json.dump(results.get_results(), out_file)
                with open(OUTPUT_DIR + model_type + "/sessions_dict", 'wb') as out_file:
                    json.dump(results.get_sessions(), out_file)
                t = time.time()


        batches.close_file()

def recip_recall(metrics, best_predictions, labels, outcome):
    rr = metrics.reciprocal_rank(best_predictions,labels)
    recall = metrics.recall_at_r(rr)
    outcome.append(rr)
    outcome.append(recall)

def get_dcg(metrics, best_predictions, labels, outcome):
    d_dcg,lcs_dcg = metrics.calculate_dcgs(best_predictions, labels)
    outcome.append(d_dcg)
    outcome.append(lcs_dcg)

def get_best_n(scores, n, sequences):
    sequences = np.squeeze(sequences[:,0,:])
    best_predictions = []
    for val in scores:
        best = sorted(scores[val].items(), key=itemgetter(1),reverse=True)
        best_arrays = []
        for i in xrange(n):
            best_arrays.append(sequences[best[i][0]])
        best_predictions.append(best_arrays)
    return np.asarray(best_predictions)

def pretty_print(out_file, perpl):
    out_file.write("-------------\n")
    for i in xrange(len(perpl)):
        out_file.write("| %d | %.3f |\n" % (i+1, perpl[i]))
    out_file.write("-------------\n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run Sequential LSTM')
    parser.add_argument('--model_type', type=str, default="sequential", help="Model type")
    parser.add_argument('--operation', type=str, default = 'train', help='Whether to train or to test')
    parser.add_argument('--model', type=str, default=None, help='Saved pre-trained model from earlier training')
    args = parser.parse_args()
    if args.operation == 'train':
        train(args.model_type, args.model)
    elif args.operation == 'test':
        test(args.model_type, args.model)
    else:
        print( "Please use --operation train or --operation test to specify the operation")
