
##### HOW TO RUN:

The training configuration should be set in `config.py`. 

To run the actual training sequence, the following code should be executed.

```bash
python main.py --model_type [sequential/non_sequential], --operation [train/test] --model checkpoint_file
```

The model is optional, not providing a checkpoint file will make the algorithm instantiate the parameters randomly

Other parameters can be found and adjusted in config.py.

- The output during testing will be stored in `OUTPUT_DIR/model_type/`
- The logs during training will be stored in `LOG_DIR/model_type/`
- The checkpoints will be stored in `CHECKPOINT_DIR/model_type`

##### INFO:
- Learning rate is set to 0.0001

##### CONTRIBUTIONS:
- Martijn
    - Implementation
    - Working out details of algorithm
        e.g.: how to initialize final lstm with output of forward-backward lstm
    - Thinking of Damerau-Levenshtein distance as non-sequential metric


- Alexey
    - Main idea
    - Advising on implementation and bug fixes
    - Providing processed data


- Ilya
    - Advising on model and metrics
