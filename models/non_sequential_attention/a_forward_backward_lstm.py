import tensorflow as tf


class FB_LSTM :
    def __init__(self, lstm_size, batch_size, lstm_type):
        self.lstm_size = lstm_size
        self.lstm_type = lstm_type
        self.batch_size = batch_size

    def inference(self, data, batch_size):
        with tf.variable_scope(self.lstm_type):
            cell = tf.nn.rnn_cell.LSTMCell(self.lstm_size,state_is_tuple=True)
            output, state = tf.nn.dynamic_rnn(cell, data, dtype=tf.float32)
            c,h = state
            output = tf.squeeze(tf.slice(output, [0,1,0], [batch_size,10,self.lstm_size]))
        return output, c

    def reverse_data(self, data):
        q0_0d = tf.slice(data,[0,0,0],[self.batch_size,1,21504])
        qd = tf.slice(data,[0,1,0],[self.batch_size,10,21504])
        qd = tf.reverse(qd,[False,True,False])
        qd = tf.concat(1,[q0_0d,qd])
        return qd

'''
print qd.get_shape()
q = tf.slice(qd,[0,0,0],[self.batch_size,10,1024])
d = tf.slice(qd,[0,0,1024],[self.batch_size,10,20480])
d = tf.reverse(d,[False,True,False])
qd = tf.concat(2,[q,d])
'''
