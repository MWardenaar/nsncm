import tensorflow as tf


class DynamicAttention:
    def __init__(self, final_lstm_size, fwbw_lstm_size, bs = 64):
        self.batch_size = bs
        self.lstm_size = final_lstm_size
        self.fwbw_lstm_size = fwbw_lstm_size
        self.cell = tf.nn.rnn_cell.LSTMCell(self.lstm_size, state_is_tuple=True)

    def inference(self, fw_hidden, bw_hidden, fw_cell, bw_cell, labels):
        first_9 = tf.slice(labels,[0,0,0],[self.batch_size,9,11])
        data = tf.concat(1, [tf.zeros([self.batch_size,1,11]),first_9])

        # Projects the source vectors (2x(64,10,256)=(64,10,512)) to same dimensions as target vector (64,10,256)
        Whs = tf.get_variable("project_hs", [2*self.fwbw_lstm_size,self.lstm_size], initializer = tf.contrib.layers.xavier_initializer())
        # Projects concatenation of context vector and target vector to (64, 256)
        Wht = tf.get_variable("weights_ht", [2*self.fwbw_lstm_size,self.lstm_size], initializer = tf.contrib.layers.xavier_initializer())
        # Weights of output layer
        Wout = tf.get_variable("weights_out", [self.lstm_size,11], initializer = tf.contrib.layers.xavier_initializer())
        bout = tf.get_variable("biases_out", [1,11], initializer=tf.constant_initializer(0.0))
        # Projects concatenated forward and backward final hidden states to shape of state of final LSTM
        W_h = tf.get_variable("Weights_hidden_init", [2*self.fwbw_lstm_size, self.lstm_size], initializer = tf.contrib.layers.xavier_initializer())
        # Projects concatenated forward and backward final cell states to shape of state of final LSTM
        W_c = tf.get_variable("Weights_cell_init", [2*self.fwbw_lstm_size, self.lstm_size], initializer = tf.contrib.layers.xavier_initializer())

        fwbw_hidden =  tf.concat(2,[fw_hidden,bw_hidden])
        fwbw_hidden = tf.slice(fwbw_hidden,[0,9,0],[self.batch_size, 1, 2*self.fwbw_lstm_size])
        m_state = tf.matmul(tf.squeeze(fwbw_hidden), W_h)
        fwbw_cell = tf.concat(1,[fw_cell,bw_cell])
        c_state = tf.matmul(fwbw_cell,W_c)

        cell = tf.nn.rnn_cell.LSTMCell(self.lstm_size, state_is_tuple=True)
        init_state = tf.nn.rnn_cell.LSTMStateTuple(c=c_state, h=m_state) # c-state (cell), m-state (hidden state)
        h_t, state = tf.nn.dynamic_rnn(cell, data, dtype=tf.float32,initial_state=init_state)

        h_s  = tf.concat(2,[fw_hidden,tf.reverse(bw_hidden,[False,True,False])])
        h_s = self.project_hs(h_s, Whs)

        distr = []
        h_t = tf.transpose(h_t,[1,0,2])
        for i in xrange(10):
            c = self.context(tf.gather(h_t,i), h_s)
            h_out = tf.tanh(tf.matmul(tf.concat(1,[c,tf.gather(h_t,i)]), Wht))
            distr.append(tf.nn.softmax(tf.add(tf.matmul(h_out,Wout),bout)))
        clicks = tf.pack(distr)
        clicks = tf.transpose(clicks, [1,0,2])
        return clicks

    def project_hs(self, h_s, Whs):
        h_s = tf.reshape(h_s,[self.batch_size*10, 2*self.fwbw_lstm_size])
        h_s = tf.matmul(h_s, Whs)
        h_s = tf.reshape(h_s,[self.batch_size, 10, self.lstm_size])
        return h_s

    def context(self,h_t, h_s):
        h_s = tf.transpose(h_s,[1,0,2])
        a = []
        for i in xrange(10):
            a_i = tf.diag_part(tf.matmul(h_t,tf.transpose(tf.gather(h_s,i))))
            a.append(a_i)
        a = tf.nn.softmax(tf.transpose(tf.pack(a)))
        h_s = tf.transpose(h_s,[1,0,2])
        c = tf.reduce_sum(tf.expand_dims(a,2)* h_s,1)
        return c

    def loss(self, labels, predictions):
        pl = tf.mul(predictions, labels)
        mpl = tf.log(tf.reduce_sum(pl,2))
        loss = tf.reduce_mean(mpl)
        return loss, mpl
