import numpy as np
import tensorflow as tf

from utils.sequences import Sequences
from dynamic_attention import DynamicAttention
from a_forward_backward_lstm import FB_LSTM


class NS_Attention:
    def __init__(self, batch_size, fwbw_lstm_size, final_lstm_size,max_saved = 5):
        self.batch_size = batch_size
        self.q0 = np.zeros((self.batch_size,10,1024))
        self.d0 = np.zeros((self.batch_size,1,20480))
        self.sequences = Sequences("Non_sequential")
        self.labels = tf.placeholder('float', [self.batch_size, 10,11], name='loss_labels_plch')
        self.batch = tf.placeholder('float', [self.batch_size,11,21504], name="data_plch")
        self.validation_loss = tf.placeholder('float', shape=[], name='loss')
        with tf.variable_scope("train_scope") as scope:
            self.fw_lstm = FB_LSTM(fwbw_lstm_size, self.batch_size, lstm_type = "forward")
            self.bw_lstm = FB_LSTM(fwbw_lstm_size, self.batch_size, lstm_type = "backward")
            self.final_lstm = DynamicAttention(final_lstm_size, fwbw_lstm_size, self.batch_size)
            self.reverse_batch = self.fw_lstm.reverse_data(self.batch)
            self.fw_hidden, self.fw_cell = self.fw_lstm.inference(self.batch, self.batch_size)
            self.bw_hidden, self.bw_cell = self.bw_lstm.inference(self.reverse_batch, self.batch_size)

            self.prediction = self.final_lstm.inference( self.fw_hidden, self.bw_hidden, self.fw_cell, self.bw_cell, self.labels)
            self.loss, self.losses = self.final_lstm.loss(self.labels, self.prediction)
            # Update model
            self.optimizer = tf.train.AdamOptimizer()
            self.tvars = tf.trainable_variables()
            self.gvs = self.optimizer.compute_gradients(-self.loss)
            self.capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in self.gvs]
            self.minimize = self.optimizer.apply_gradients(self.capped_gvs)
            tf.scalar_summary('loss', self.validation_loss)
            # Merger for the summaries that go into TensorBoard
            self.merged = tf.merge_all_summaries()
            # Saver to create a checkpoint of the model
            self.saver = tf.train.Saver(max_to_keep=max_saved)
            scope.reuse_variables()

    def initialize(self, sess):
        sess.run(tf.initialize_all_variables())

    def load_model(self, sess, model):
        self.saver.restore(sess, model)

    def get_predictions(self, sess, batch_data, batch_labels):
        [p] = sess.run([self.prediction],{self.labels:batch_labels, self.batch:batch_data, self.validation_loss:0 })
        return p

    def get_loss(self, sess, batch_data, batch_labels):
        [l,summary] = sess.run([self.losses,self.merged],{ self.labels:batch_labels, self.batch:batch_data, self.validation_loss:0 })
        return l,summary

    def train(self, sess, batch_data, batch_labels):
        [l,_] = sess.run([self.loss, self.minimize],{self.labels:batch_labels, self.batch:batch_data, self.validation_loss:0 })
        return l

    def save_model(self, sess, save):
        save_path = self.saver.save(sess, save)

    def add_summaries(self, sess, loss):
        [summary] = sess.run([self.merged],{self.validation_loss:loss})
        return summary

    # This function takes the data from all instances in the batch and creates one
    # big matrix which can be fed to the LSTM
    def create_data(self, q,d):
        q = np.expand_dims(q,1)
        q2 = np.concatenate((q,self.q0),1)
        d2 = np.concatenate((self.d0,d), axis=1)
        data = np.concatenate((q2,d2), 2)
        return data

    def sequence_to_set(self, sequences):
        sets = []
        for sequence in sequences:
            #print sequence
            s = np.zeros((10,1))
            for seq in sequence:
                #print seq
                index = int(np.where(seq == 1)[0])
                if index < 10:
                    s[index] = 1
                else:
                    break
            sets.append(s)
        return np.squeeze(np.asarray(sets))

    def get_sequences(self, n):
        sequences_3 = self.sequences.get_sequences(n)
        sequences_7 = np.zeros((sequences_3.shape[0], 10-sequences_3.shape[1], sequences_3.shape[2]))
        sequences = np.concatenate((sequences_3,sequences_7), 1)
        sequences = np.expand_dims(sequences,1)
        sequences = np.repeat(sequences, self.batch_size, 1)
        return sequences

    def likelihood(self, predictions, labels):
        return np.prod(np.sum(predictions[:,0:3]*labels[:,0:3],axis=2), axis=1)
