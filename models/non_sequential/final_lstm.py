import tensorflow as tf


class FINAL_LSTM :
    def __init__(self, final_lstm_size, fwbw_lstm_size, bs = 64):
        self.batch_size = bs
        self.lstm_size = final_lstm_size
        self.fwbw_lstm_size = fwbw_lstm_size

    def inference(self, fw_cell, bw_cell, fw_hidden, bw_hidden, labels):
        with tf.variable_scope("final_lstm"):
            first_9 = tf.slice(labels,[0,0,0],[self.batch_size,9,11])
            data = tf.concat(1, [tf.zeros([self.batch_size,1,11]),first_9])
            fwbw_hidden = tf.concat(1,[fw_hidden,bw_hidden])
            W_h = tf.get_variable("Weights_hidden_init", [2*self.fwbw_lstm_size, self.lstm_size], initializer =tf.contrib.layers.xavier_initializer())
            h_state = tf.matmul(fwbw_hidden, W_h)
            fwbw_cell = tf.concat(1,[fw_cell,bw_cell])
            W_c = tf.get_variable("Weights_cell_init", [2*self.fwbw_lstm_size, self.lstm_size], initializer = tf.contrib.layers.xavier_initializer())
            c_state = tf.matmul(fwbw_cell,W_c)

            cell = tf.nn.rnn_cell.LSTMCell(self.lstm_size, state_is_tuple=True)
            init_state = tf.nn.rnn_cell.LSTMStateTuple(c=c_state, h=h_state) # c-state (cell), h-state (hidden state)
            val, state = tf.nn.dynamic_rnn(cell, data, dtype=tf.float32,initial_state=init_state)

            Wout = tf.get_variable("weights_out", [self.lstm_size,11], initializer = tf.contrib.layers.xavier_initializer())
            bout = tf.get_variable("biases_out", [1,11], initializer=tf.constant_initializer(0.0))

            val = tf.transpose(val,[1,0,2])
            distr = []
            for i in xrange(10):
                mul = tf.matmul(tf.gather(val, i),Wout)
                prob_distr = tf.nn.softmax(tf.add(mul,bout))
                distr.append(prob_distr)
            clicks = tf.pack(distr)
            clicks = tf.transpose(clicks, [1,0,2])
            return clicks

    def loss(self, labels, predictions):
        pl = tf.mul(predictions, labels)
        mpl = tf.log(tf.reduce_sum(pl,2))
        loss = tf.reduce_mean(mpl)
        return loss, mpl
