import numpy as np
import tensorflow as tf

from utils.sequences import Sequences
from ncm_lstm import NCM_LSTM

class NCM:
    def __init__(self, batch_size, lstm_size,max_saved = 5):
        self.q0 = np.zeros((batch_size,10,1024))
        self.y0 = np.zeros((batch_size,2))
        self.d0 = np.zeros((batch_size,1,20480))
        self.sequences = Sequences("Sequential")
        self.batch_size = batch_size
        self.lstm_size = lstm_size
        self.y = tf.placeholder('float', shape=[None, 10], name="y_plch")
        self.batch = tf.placeholder('float', [None,11,21505], name="data_plch")
        self.validation_loss = tf.placeholder('float', shape=[], name='loss')
        with tf.variable_scope("train_scope") as scope:
            # Creating an LSTM object
            self.lstm = NCM_LSTM(self.lstm_size, self.batch_size)
            # Running the prediction
            self.prediction = self.lstm.inference(self.batch)
            self.loss, self.losses = self.lstm.loss(self.y, self.prediction)
            self.optimizer = tf.train.AdamOptimizer()
            self.tvars = tf.trainable_variables()
            self.gvs = self.optimizer.compute_gradients(-self.loss)
            self.capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in self.gvs]
            self.minimize = self.optimizer.apply_gradients(self.capped_gvs)
            tf.summary.scalar('loss', self.validation_loss)
            self.merged = tf.summary.merge_all()
            # Saver to create a checkpoint of the model
            self.saver = tf.train.Saver(max_to_keep=max_saved)
            scope.reuse_variables()

    def initialize(self, sess):
        sess.run(tf.initialize_all_variables())

    def load_model(self, sess, model):
        self.saver.restore(sess, model)

    def get_predictions(self, sess, batch_data,batch_labels):
        [p] = sess.run([self.prediction],{self.y:batch_labels, self.batch:batch_data,self.validation_loss:0.0 })
        return p

    def get_loss(self, sess, batch_data, batch_labels):
        [l,summary] = sess.run([self.losses,self.merged],{self.y:batch_labels, self.batch:batch_data, self.validation_loss:0.0 })
        return l,summary

    def train(self, sess, batch_data, batch_labels):
        [l,_] = sess.run([self.loss, self.minimize],{self.y:batch_labels, self.batch:batch_data, self.validation_loss:0.0 })
        return l

    def save_model(self, sess, save):
        save_path = self.saver.save(sess, save)

    def add_summaries(self, sess, loss):
        [summary] = sess.run([self.merged],{self.validation_loss:loss})
        return summary

    def create_data(self, q, d,y):
        q = np.expand_dims(q,1)
        q2 = np.concatenate((q,self.q0), 1)
        y = np.concatenate((self.y0,y[:,0:9]), 1)
        y = np.expand_dims(y,2)
        d = np.concatenate((self.d0,d),1)
        data = np.concatenate((q2,y,d), 2)
        return data


    def bits_to_int(self, l):
        return int("".join(str(i) for i in l.astype(int)[0]), 2)

    def int_to_bits(self, n):
        return np.asarray(map(int,list('{0:010b}'.format(n))))

    def get_sequences(self,n):
        s = self.sequences.get_sequences(n)
        s = np.squeeze(s)
        s = np.expand_dims(s, 1)
        s = np.repeat(s, self.batch_size, 1)
        return s

    def likelihood(self, predictions, labels):
        return np.prod(predictions*labels + (1.0-predictions)*(1.0-labels), axis=1)
