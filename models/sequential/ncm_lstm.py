import tensorflow as tf

class NCM_LSTM :
    def __init__(self, lstm_size,batch_size = 64):
        self.batch_size = batch_size
        self.lstm_size = lstm_size

    def inference(self,data):
        Wout = tf.get_variable("weights_out", [self.lstm_size,1], initializer = tf.contrib.layers.xavier_initializer())
        bout = tf.get_variable("biases_out", [1,1], initializer=tf.constant_initializer(0.0))
        cell = tf.nn.rnn_cell.LSTMCell(self.lstm_size,state_is_tuple=True)
        val, state = tf.nn.dynamic_rnn(cell, data, dtype=tf.float32, time_major=False)
        
        val = tf.transpose(val,[1,0,2])
        clicks = []
        for i in xrange(1,11):
            click = tf.matmul(tf.gather(val, i),Wout)
            click = tf.sigmoid(click+bout)
            clicks.append(tf.squeeze(click,[1]))
        clicks = tf.stack(clicks, axis=1)
        return clicks

    def loss(self, labels, predictions):
        mult = (predictions * labels) + ((1.0-labels) * (1.0-predictions))
        log_mult = tf.log(mult)
        loss = tf.reduce_mean(log_mult)
        losses = tf.reduce_mean(log_mult, 1)
        return loss, losses
